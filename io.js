const fs = require('fs'); //usamos la librería fs que hemos instalado desde consola con: npm install fs -save

function escribirUsuarioDatoFichero(data){
  console.log("escribirUsuarioDatoFichero");

  var jsonUserData = JSON.stringify(data);

  fs.writeFile('./usuarios.json',jsonUserData,"utf8",
    function(err){
      if(err){
        console.log(err);
      }else{
        console.log("Usuario Persistido");
      }
    }
  );
}

module.exports.escribirUsuarioDatoFichero = escribirUsuarioDatoFichero;
