require('dotenv').config(); //carga las variables de entorno definidas en el fichero .env; que por defecto coge del raiz

const express = require('express'); //required carga la libreria express
const app = express(); //inicializamos el framework

const port = process.env.PORT || 3000; //abrimos el puerto del servidos
app.use(express.json()); //con esto consigo que se haga un preprocesado del body (asume que es jason y lo intenta parsear como tal)
                          //es similar a que estuvieramos haciendo nosotros el parseo del body a json

var enableCORS = function(req, res, next) {
  res.set("Access-Control-Allow-Origin", "*");
  res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
  res.set("Access-Control-Allow-Headers", "Content-Type");
  next();
}
app.use(enableCORS); //Añadimos las cabeceras que neceita el navegador para interpretar el código


const userController = require("./controller/UserController")
const authController = require("./controller/AuthController")
const accountController = require("./controller/AccountController")

app.listen(port); //lanzamos el servidor en el puerto indicado

console.log("API escuchando en el puerto: " + port);

app.get('/apitechu/v1/hello',
  function(req,res){   //req es el objeto de entrada y res el de salida
    console.log(" GET /apitechu/v1/hello");
    res.send({"msg": "Hola desde el servidor"});
  }
)//se indica la ruta y a función manejadora de esa ruta que es la que va a hacer el trabajo

app.get('/apitechu/v1/users',userController.getUsersV1);
app.post('/apitechu/v1/users',userController.postUsersV1);
app.delete("/apitechu/v1/users/:id",userController.deleteUsersV1);

app.post("/apitechu/v1/login",authController.postLoginV1);
app.post("/apitechu/v1/logout/:id",authController.postLogoutV1);

app.get('/apitechu/v2/users',userController.getUsersV2);
app.get('/apitechu/v2/users/:id',userController.getUserByIdV2);
app.post('/apitechu/v2/users',userController.createUserV2);
app.post("/apitechu/v2/login",authController.postLoginV2);
app.put("/apitechu/v2/user/:id",userController.putUserV2);
app.post("/apitechu/v2/logout/:email",authController.postLogoutV2);

app.get("/apitechu/v2/accounts/:id",accountController.accountController);

app.post("/apitechu/v1/users/monstruo/:p1/:p1",    //REspondera cuando se llame con los dos parametros indicados, p1 y p2 son obligatorios
  function(req,res){
      console.log("Parametros");
      console.log(req.params);

      console.log("Querey");
      console.log(req.query);

      console.log("Headers");
      console.log(req.headers);

      console.log("Body");
      console.log(req.body);
  }
)
