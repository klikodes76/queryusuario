const io = require("../io.js")
const basrMLabURL = "https://api.mlab.com/api/1/databases/apitechujcg10ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY
const requestJson = require("request-json")
const bcrypt = require('bcrypt');

function postLogoutV2(req,res){
  var httpClient = requestJson.createClient(basrMLabURL)

  console.log('user?' + 'q={"email":"' + req.params.email + '"}&' + mLabAPIKey);
  httpClient.get('user?' + 'q={"email":"' + req.params.email + '"}&' + mLabAPIKey,
  function(err,reMLab,boby) {
      if(err) {
        var response = {"msg": "Error obteniedo usuarios"}
        res.status(500);
      }else{
        console.log("Respuesta:")
        console.log(boby)
        if (boby.length > 0) {
          var response = boby[0];
        }else{
          var response = {"msg": "Usuario no encontrado"}
          res.status(404);
        }
      }
      //actualizo indicador de logueo
      if (boby[0].logged){

        var query = 'q={"email": "' + req.params.email + '"}'
        var putBody = '{"$unset":{"logged":""}}'

        httpClient.put("user?" + query + "&" + mLabAPIKey,JSON.parse(putBody),function(err1,reMLab1,boby1){

          var response  = !err ? {"msg": "Usuario desloggado"} : {"msg": "Error actualizando usuario"}
          res.send(response);
        })

      }else{
        res.send({"msg": "Usuario no loggado"});
      }

    }
  )
}

function postLoginV2(req,res){
  console.log("email" + req.body.email + "pss: " + req.body.password);

  var httpClient = requestJson.createClient(basrMLabURL)

  var httpClient = requestJson.createClient(basrMLabURL);
  console.log("Cliente creado");

  let userfind = false;

  httpClient.get("user?" + mLabAPIKey,
    function(err,reMLab,boby) {
        var response  = !err ? boby : {
          "msg": "Error obteniedo usuarios"
        }

        response.forEach(function(user,i){
          if(user.email == req.body.email){
            userfind  = true;
            console.log("ha pasado poraaaaqui" + userfind);

            if (bcrypt.compareSync(req.body.password, user.password)){

              var query = 'q={"email": "' + req.body.email + '"}'
              var putBody = '{"$set":{"logged":true}}'; //con $set añadimos una propiedad a un objeto

              httpClient.put("user?" + query + "&" + mLabAPIKey,JSON.parse(putBody),function(err1,reMLab1,boby1){

                var response  = !err ? {"msg": "Usuario loggado","idUsuario": user.id} : {"msg": "Error actualizando usuario"}

                res.send(response);
              })

            }else{
              res.send({"msg": "pss incorrecta"});
              res.status(401);
            }
          }

        })
        if (!userfind){
          res.send({"msg": "Usuario no encontrado"});
          res.status(401);
        }
    }
  )

};


function postLoginV1(req,res){

 var users = require('../usuarios.json');
 var busqueda = false;
 var id;

 users.forEach(function(user,i){
   //console.log(user.email + user.password);
   if(user.email == req.body.email && user.password == req.body.password){
     users[i].logged = true;
     console.log(user);
     io.escribirUsuarioDatoFichero(users);
     busqueda = true;
     id = user.id
   }
 })

 console.log(busqueda);
 if(busqueda){
   res.send({"mensaje" : "login correcto","idUsuario" : id});
 }
 else{
   res.send({"mensaje" : "login incorrecto"});
 }

};

function postLogoutV1(req,res){

 var users = require('../usuarios.json');
 var busqueda = false;
 var id=req.params.id;


 users.forEach(function(user,i){
   //console.log(user.email + user.password);
   if(user.id == id && user.logged){
     //users[i].logged = false;
     delete users[i].logged;
     console.log(user);
     io.escribirUsuarioDatoFichero(users);
     busqueda = true;
   }
 })


if(busqueda){
  res.send({"mensaje" : "logout correcto","idUsuario" : id});
}
else{
  res.send({"mensaje" : "logout incorrecto"});
}
// var msg = busqueda ? "Logout correcto id: "+ id : "Logout incorrecto"
// res.send({"msg":msg});

};

module.exports.postLoginV1 = postLoginV1;
module.exports.postLogoutV1 = postLogoutV1;
module.exports.postLoginV2 = postLoginV2;
module.exports.postLogoutV2 = postLogoutV2;

/*LOGINV2
function loginV2(req, res) {
 console.log("POST /apitechu/v2/login");  var query = 'q={"email": "' + req.body.email + '"}';
 console.log("query es " + query);  httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {      var isPasswordcorrect =
       crypt.checkPassword(req.body.password, body[0].password);
     console.log("Password correct is " + isPasswordcorrect);      if (!isPasswordcorrect) {
       var response = {
         "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
       }
       res.status(401);
       res.send(response);
     } else {
       console.log("Got a user with that email and password, logging in");
       query = 'q={"id" : ' + body[0].id +'}';
       console.log("Query for put is " + query);
       var putBody = '{"$set":{"logged":true}}';
       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
           var response = {
             "msg" : "Usuario logado con éxito",
             "idUsuario" : body[0].id
           }
           res.send(response);
         }
       )
     }
   }
 );
}

*LOGOUT V2*
function logoutV2(req, res) {
 console.log("POST /apitechu/v2/logout/:id");  var query = 'q={"id": ' + req.params.id + '}';
 console.log("query es " + query);  httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
     if (body.length == 0) {
       var response = {
         "mensaje" : "Logout incorrecto, usuario no encontrado"
       }
       res.send(response);
     } else {
       console.log("Got a user with that id, logging out");
       query = 'q={"id" : ' + body[0].id +'}';
       console.log("Query for put is " + query);
       var putBody = '{"$unset":{"logged":""}}'
       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
           var response = {
             "msg" : "Usuario deslogado",
             "idUsuario" : body[0].id
           }
           res.send(response);
         }
       )
     }
   }
 );
} */
