const io = require("../io.js")
const requestJson = require("request-json")
const basrMLabURL = "https://api.mlab.com/api/1/databases/apitechujcg10ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY
const crypt = require('../crypt')

function accountController(req,res){
  console.log("/apitechu/v2/account/user/:id");
  var httpClient = requestJson.createClient(basrMLabURL)


  httpClient.get('cuentas?' + 'q={"IdUsuario":' + req.params.id + '}&' + mLabAPIKey,
  function(err,reMLab,boby) {
      if(err) {
        var response = {"msg": "Error obteniedo cuentas"}
        res.status(500);
      }else{
        if (boby.length > 0) {
          var response = boby;
        }else{
          var response = {"msg": "Usuario no encontrado"}
          res.status(404);
        }
      }
      res.send(response);

    }
  )
}

module.exports.accountController = accountController;
