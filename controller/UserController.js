const io = require("../io.js")
const requestJson = require("request-json")
const basrMLabURL = "https://api.mlab.com/api/1/databases/apitechujcg10ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY
const crypt = require('../crypt')

function putUserV2(req,res){
  console.log("he llegado");
}


function createUserV2(req,res){
  console.log(" POST /apitechu/v2/users");

  console.log(req.body.id)
  console.log(req.body.first_name)
  console.log(req.body.last_name)
  console.log(req.body.email)
  console.log(req.body.password)

  var newUser = {
  "id":req.body.id,
  "first_name":req.body.first_name,
  "last_name":req.body.last_name,
  "email":req.body.email,
  "password": crypt.hash(req.body.password)};

  var httpClient = requestJson.createClient(basrMLabURL)

  httpClient.post('user?' + mLabAPIKey, newUser,
  function(err,reMLab,boby) {
      console.log("Usuario creado en Mlab");
      res.status(201).send({"msg":"Usuario guardado"});
  }
)
}

function getUserByIdV2(req,res){
  console.log("/apitechu/v2/users/:id");
  var httpClient = requestJson.createClient(basrMLabURL)

  httpClient.get('user?' + 'q={"id":' + req.params.id + '}&' + mLabAPIKey,
  function(err,reMLab,boby) {
      if(err) {
        var response = {"msg": "Error obteniedo usuarios"}
        res.status(500);
      }else{
        if (boby.length > 0) {
          var response = boby[0];
        }else{
          var response = {"msg": "Usuario no encontrado"}
          res.status(404);
        }
      }
      res.send(response);

    }
  )
}

function getUsersV2(req,res){
  console.log(" GET /apitechu/v2/users");
  var httpClient = requestJson.createClient(basrMLabURL);
  console.log("Cliente creado");

  httpClient.get("user?" + mLabAPIKey,
    function(err,reMLab,boby) {
        var response  = !err ? boby : {
          "msg": "Error obteniedo usuarios"
        }
    res.send(response);
    }
  )

}

function getUsersV1(req,res){
  console.log(" GET /apitechu/v1/users");

  console.log("esta es la query top" + req.query.$top);
  console.log("esta es la query count" + req.query.$count);

  var users = require('../usuarios.json');
  var newUser = {};

  if (req.query.$count){
    if (req.query.$top){
        newUser = {
          "count": users.length,
          "user": users.slice(0,req.query.$top)  //result.user=req.query.$top ? user.slice(0,req.query.$top):user;
        }
    }else{
      newUser = {
        "count": users.length,
        "user": users
      }
    }
    res.send(newUser);
  }else{
    if (req.query.$top){
        res.send(users.slice(0,req.query.$top));
    }else{
        res.send(users);
    }
  }
  //res.sendFile('usuarios.json',{root:__dirname})//__dirname -> vriable de entorno que indica donde está el scrip
}

function postUsersV1(req,res){
  //console.log(" POST /apitechu/v1/users");

  //console.log(req.body.first_name);
  //console.log(req.body.last_name);
  //console.log(req.body.email);

  var newUser = {
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email
  }
  var users = require('../usuarios.json');
  users.push(newUser);
  console.log("Usuario añadido con exito");

  io.escribirUsuarioDatoFichero(users);

  res.send({"msg" : "Usuario añadido"})

}

function deleteUsersV1(req,res){
  console.log("Borrado a tope");
  console.log("id: " + req.params.id);
  var users = require('../usuarios.json');

/* Metodo iteracion uno
  for (var i = 0;i<users.length;i++){
    if(users[i].id == req.params.id){
      console.log("Borrado en iteración i: " + i);
      users.splice(i, 1);
      break;
    }
  }
*/
//Metodo iteracion dos
//Ojo el for each no permite el break, por lo que no es optimo para hcer una busqueda.
/*
users.forEach(function(item,i){
  if(item.id == req.params.id){
    users.splice(i, 1)
  }
})
*/
//Metodo iteracion tres
//Itera sobre las propiedades de un objeto, obtiene la id del array.
//No es la apropiada para trabajar con un array
/*
for (const prop in users) {

if (users[prop].id ==req.params.id){
  console.log(`users.${prop} = ${users[prop].id}`);
  users.splice(prop, 1);
  break;
}
}
*/
//Metodo iteracion cuatro
//Reciorre el array pero no tiene indice
// se puede usar user.entries()
//for (var [index,user] of users.entries()) -> de esta forma tienes acceso al contador(index) y al elemento user
/*
var i = 0;
for (const prop of users) {
if (prop.id ==req.params.id){
  users.splice(i, 1);
  break;
}
i = i + 1;
}
*/
//Metodo cinco
/*  var i = users.findIndex(encontrarId)
function encontrarId(element, index, array){
  if (element.id == req.params.id){
    return index;
  }
}*/

var i = users.findIndex(function (element, index, array){
    if (element.id == req.params.id)
    {
      return index;
    }
})
//cuando nuestra función devuelve true, findIndex devuelve el elemento donde se ha producido
/*
var indexOfElement = users.findIndex(
     function(element){
       console.log("comparando " + element.id + " y " +   req.params.id);
       return element.id == req.params.id
     }

   )
*/

users.splice(i, 1);

   //
io.escribirUsuarioDatoFichero(users);
       //var msg = deleted ? "usuario borrado" : "usuario no encontrado"
       res.send({"msg":"Usuario borrado de la lista"});
}

module.exports.deleteUsersV1 = deleteUsersV1;
module.exports.postUsersV1 = postUsersV1;
module.exports.getUsersV1 = getUsersV1;   //esto se pone para que se "exponga" la función desde fuera
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUserV2 = createUserV2;
module.exports.putUserV2 = putUserV2;
